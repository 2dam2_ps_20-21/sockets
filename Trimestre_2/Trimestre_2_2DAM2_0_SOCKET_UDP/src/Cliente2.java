import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Cliente2 {
	public static void main(String[] args) {
		
		final int PUERTO = 500;
		byte[] buffer = new byte[1024];
		
		try {
			
			
			while(true) {
				System.out.println("Iniciado cliente 2");
				DatagramSocket socketUDP = new DatagramSocket(PUERTO);
				
				DatagramPacket recepcion = new DatagramPacket(buffer, buffer.length);
				socketUDP.receive(recepcion);
				System.out.println("Recepcion realizada");
				
				String mensaje = new String (recepcion.getData());
				System.out.println("Mensaje: " + mensaje);
				
				////////////////////////////
				
				int puertoCliente1 = recepcion.getPort();
				InetAddress direccion = recepcion.getAddress();
				
				mensaje = "Mensaje del cliente2";
				buffer = mensaje.getBytes();
				
				DatagramPacket envio = new DatagramPacket(buffer, buffer.length, direccion, puertoCliente1);
				System.out.println("Envio realizado de cliente 2 a cliente 1");
				socketUDP.send(envio);
			}
		}catch(Exception e) {
			
		}
	}
}
