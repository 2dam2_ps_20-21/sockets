import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Cliente1 {
	public static void main(String[] args) {
		
		final int PUERTO = 500;
		byte[] buffer = new byte [1024];
		
		try {
			InetAddress direccion = InetAddress.getByName("localhost");
			DatagramSocket socketUDP = new DatagramSocket();
			
			String mensaje = "HOLA SOY EL CLIENTE 1";
			buffer = mensaje.getBytes();
			
			DatagramPacket envio = new DatagramPacket(buffer, buffer.length, direccion, PUERTO);
			System.out.println("Envio del datagrama");
			
			socketUDP.send(envio);
			
			/////////////////////////////////////
			
			DatagramPacket recepcion = new DatagramPacket(buffer, buffer.length);
 			socketUDP.receive(recepcion);
 			
 			System.out.println("Recibo datagrama");
 			mensaje = new String(recepcion.getData());
 			
 			System.out.println(mensaje);
 			socketUDP.close();
		}catch(Exception e) {
			
		}
	}
}
