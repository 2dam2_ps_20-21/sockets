package ejemplo;

public class Main {

	public static void main(String[] args) {
		Motor m = new Motor();
		Acelerador a = new Acelerador();
		Motor m2 = new Motor();
		
		a.enlazarObservador(m);
		a.enlazarObservador(m2);
		a.activarAcelerador();
	}
}
