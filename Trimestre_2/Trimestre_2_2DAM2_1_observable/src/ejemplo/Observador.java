package ejemplo;

public interface Observador { // Motor
	public void update(); // Actualizar cuando el sujeto observado (motor) dispare un evento
}
