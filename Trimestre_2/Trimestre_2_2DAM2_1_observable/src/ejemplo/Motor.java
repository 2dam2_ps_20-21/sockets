package ejemplo;

public class Motor implements Observador{

	public Motor() {}
	
	/* Accion que voy a realizar cuando el sujetoObservable (acelerador) realice un evento  */
	@Override
	public void update() {
		System.out.println("Se ha realizado un evento");
	}
	
}
