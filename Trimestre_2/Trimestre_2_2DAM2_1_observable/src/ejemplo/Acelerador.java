package ejemplo;

import java.util.ArrayList;

public class Acelerador implements sujetoObservado {

	/* Tenemos que enlazar acelerador y motor */
	/* Creo un arraylist para los observables */
	
	private ArrayList<Observador> observadores;
	
	/* Constructor */
	public Acelerador() {
		this.observadores = new ArrayList<Observador>();
	}

	// activarAcelerador --> llamar a notificar
	public void activarAcelerador() {
		notificar();
	}
	
	// enlazarObservador --> enlaza con los observadores de mi arraylist
	public void enlazarObservador (Observador o) {
		observadores.add(o);
	}
	
	/* Realiamos un for por cada observador de mi arraylist para notificar
	 * ese cambio con el metodo update */
	@Override
	public void notificar() {
		for (Observador obs: observadores) {
			obs.update();
		}
	}
}
