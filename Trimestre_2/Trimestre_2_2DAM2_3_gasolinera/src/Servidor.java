import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor implements Runnable {
	private ArrayList<Socket> clientes;
	
	private int puerto;
	
	
	public Servidor(int puerto) {
		super();
		this.puerto = puerto;
		this.clientes = new ArrayList();
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		ServerSocket servidor = null;
		Socket cliente = null;
		
		try {
			//Crear socket servidor
			servidor = new ServerSocket(puerto);
			
			//Aceptar peticion de conexion del cliente
			cliente = servidor.accept();
			
			//Guardar en mi arraylist el cliente
			clientes.add(cliente);
		} catch (IOException e) {	e.printStackTrace();
		}
	}
	
	//Metodo para enviar variaciones de los precios
	public void enviarInfo(String[] nombres, double[] valores) {
		for(Socket cliente: clientes) {
			try {
				DataOutputStream out = new DataOutputStream(cliente.getOutputStream());
				for(int i = 0; i < nombres.length; i++) {
					out.writeUTF(nombres[i]);
					out.writeDouble(valores[i]);
				}
				
			} catch (IOException e) {}
			
		}
	}

}
