import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;

public class Cliente extends Observable implements Runnable{
	
	private int puerto;
	
	public Cliente(int puerto) {
		this.puerto = puerto;
	}
	
	@Override
	public void run() {
		final String HOST = "127.0.0.1";
		DataInputStream in;
		
		try {
			Socket cliente = new Socket(HOST, puerto);
			in = new DataInputStream(cliente.getInputStream());
			
			String nombre;
			double valor;
			
			while(true) {
				nombre = in.readUTF();
				
				this.setChanged();
				this.notifyObservers(nombre);
				this.clearChanged();
				
				valor = in.readDouble();
				
				this.setChanged();
				this.notifyObservers(valor);
				this.clearChanged();
			}
		} catch (UnknownHostException e) {} catch (IOException e) {}
		
	}
	
}
