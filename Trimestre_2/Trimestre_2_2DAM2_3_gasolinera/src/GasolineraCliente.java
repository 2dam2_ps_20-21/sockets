import java.util.Observable;
import java.util.Observer;

public class GasolineraCliente extends javax.swing.JFrame implements Observer{
	
	private Cliente cliente;
	
	private String nombreProducto;
	
	private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    
    private javax.swing.JTextField txtDiesel;
    private javax.swing.JTextField txtDieselOptima;
    private javax.swing.JTextField txtGasolinaPlomo;
    
	public GasolineraCliente() {
		//1. Iniciar componentes graficos
		initComponents();
		
		//2. Inicializar cliente con puerto
		cliente = new Cliente(6000);
		
		//3. Agregar observer
		cliente.addObserver(this);
		
		//4. Lanzar un thread de cliente
		Thread thread = new Thread(cliente);
		thread.start();
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		//1. arg1 es String???? Si es String, guardo en una variable --> nombre del producto
		if(arg1 instanceof String) {
			nombreProducto = (String) arg1;
		}
		//2. arg1 es double??? Si es double, comparo con el nombre del producto y lo introduzco
		else {
			switch(nombreProducto) {
			case "diesel":
				this.txtDiesel.setText((double)arg1+"");
				break;
			case "gasolina":
				this.txtGasolinaPlomo.setText((double)arg1+"");
				break;
			case "optima":
				this.txtDieselOptima.setText((double)arg1+"");
				break;
			}
		}
	}
	
	public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {} catch (InstantiationException ex) {} catch (IllegalAccessException ex) { } catch (javax.swing.UnsupportedLookAndFeelException ex) {}

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GasolineraCliente().setVisible(true);
            }
        });
    }
	@SuppressWarnings("unchecked")
    private void initComponents() {
        txtDieselOptima = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtDiesel = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtGasolinaPlomo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Cliente");

        txtDieselOptima.setEditable(false);
        jLabel1.setText("Disesel");
        
        txtDiesel.setEditable(false);
        jLabel2.setText("Gasolina plomo");
        
        txtGasolinaPlomo.setEditable(false);
        jLabel3.setText("Diesel optima");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(33, 33, 33)
                        .addComponent(txtDiesel, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addGap(33, 33, 33)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtGasolinaPlomo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDieselOptima, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(56, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtDiesel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtGasolinaPlomo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtDieselOptima, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        pack();
    }
}
