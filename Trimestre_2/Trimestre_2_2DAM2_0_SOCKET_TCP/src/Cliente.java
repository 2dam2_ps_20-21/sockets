import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Cliente {
	
	/* SOCKET CLIENTE TCP */
	public static void main (String[] args) {
		final String HOST = "127.0.0.1"; // Hace referencia a mi propia maquina
		final int PUERTO = 500; // Mismo puerto que en el servidor
		
		DataInputStream input;
		DataOutputStream output;
		
		try {
			Socket soc = new Socket(HOST, PUERTO);
			
			input = new DataInputStream(soc.getInputStream());
			output = new DataOutputStream(soc.getOutputStream());
			
			/* ENVIO */
			output.writeUTF("Hola soy el cliente y te envio un mensaje");
			
			/* RECEPCION */
			String mensaje = input.readUTF();
			System.out.println(mensaje);
			
			soc.close();
			
		} catch (IOException e) {}
	}
}
