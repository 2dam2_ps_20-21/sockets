import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
	
	/* SOCKET SERVIDOR TCP */
	public static void main(String[] args) {
		
		DataInputStream input;
		DataOutputStream output;
		final int PUERTO = 500;
		
		try {
			ServerSocket servidor = new ServerSocket(PUERTO);
			System.out.println("Servidor iniciado");
			
			/* Listen */
			while(true) {
				Socket cliente = servidor.accept();
				
				input = new DataInputStream(cliente.getInputStream());
				output = new DataOutputStream(cliente.getOutputStream());
				
				/* Mensaje de llegada */
				String mensaje = input.readUTF();
				System.out.println(mensaje);
				
				/* Respuesta del servidor */
				output.writeUTF("HOLA SOY EL SERVIDOR Y TE RESPONDO");
				cliente.close();
				System.out.println("El cliente se ha desconectado");
				
			}
			
		} catch (IOException e) {}
	}
}
