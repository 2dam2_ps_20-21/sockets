import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Cliente {
	public static void main (String [] args) throws IOException {

		//Creamos un socket multicast en el puerto 10000:
		MulticastSocket s = new MulticastSocket (10000);
		
		//Configuramos el grupo (IP) a la que nos conectaremos:
		InetAddress group = InetAddress.getByName ("231.0.0.1");

		//Nos unimos al grupo:
		s.joinGroup (group);
		
		// Los paquetes enviados son de 256 bytes de maximo 
		//(es adaptable)
		byte [] buffer = new byte [256];
		
		// Recibimos el paquete del socket:
		DatagramPacket dgp = new DatagramPacket (buffer, buffer.length);
		s.receive (dgp);

		//Salimos del grupo multicast
		s.leaveGroup (group);

		// Cerramos el socket:
		//s.close (); 

	}
}
