import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Servidor {
	public static void main (String[] args) throws IOException {

		System.out.println ("Arrancando el servidor multicast...\n");
	
		//Creamos el MulticastSocket sin especificar puerto.
		MulticastSocket s = new MulticastSocket ();

		//Creamos el grupo multicast:
		InetAddress group = InetAddress.getByName ("231.0.0.1");
		
		while(true) {
			// Creamos un datagrama vac�o en principio:
			byte [] vacio = new byte [0];

			//Crear el Datagrama (mensaje, tama�o msj, grupo Multicast y puerto):
			DatagramPacket dgp = new DatagramPacket (vacio, 0, group, 10000);

			//Enviamos el paquete
			s.send (dgp);

		}
		
		//Cerramos el socket:
		//s.close ();
	}
}
